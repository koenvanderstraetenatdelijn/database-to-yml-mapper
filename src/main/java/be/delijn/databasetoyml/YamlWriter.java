package be.delijn.databasetoyml;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class YamlWriter {
    public void writeDataToYaml(String outputFileName, String tableName, List<Map<String, String>> data) {
        Path path = Paths.get("src/main/resources/" + outputFileName);

        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(path))) {
            printTableHeader(writer, tableName);
            printTableData(writer, data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printTableData(PrintWriter writer, List<Map<String, String>> data) {
        for (Map<String, String> rowData : data) {
            boolean firstColumn = true;

            for (Map.Entry<String, String> entry : rowData.entrySet()) {
                printTableValues(writer, firstColumn, entry.getKey(), entry.getValue());
                firstColumn = false;
            }
        }
    }

    private void printTableHeader(PrintWriter writer, String tableName) {
        writer.println(tableName + ":");
    }

    private void printTableValues(PrintWriter writer, boolean firstColumn, String columnName, String value) {
        String printableValue = ("NULL".equalsIgnoreCase(value) ? value : "\"" + value + "\"");

        if (firstColumn) {
            writer.println("  - " + columnName + ": " + printableValue);
        } else {
            writer.println("    " + columnName + ": " + printableValue);
        }
    }

}
