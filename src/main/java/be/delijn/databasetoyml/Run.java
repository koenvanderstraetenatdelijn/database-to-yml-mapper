package be.delijn.databasetoyml;

public class Run {
    public static void main(String[] args) {
        String tableName = "FactuurAfwijking";
        String inputCsvFile = "input/input.csv";
        String outputYmlFile = "output/" + tableName + ".yml";
        String csvSeparator = "\\t";

        new DataMapper(tableName, inputCsvFile, outputYmlFile, csvSeparator)
                .mapDataFromCsvToJson();
        System.out.println("Data has been written to " + outputYmlFile);
    }

}
