package be.delijn.databasetoyml;

import org.apache.commons.lang3.StringUtils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DataMapper {
    private final String inputCsvFile;
    private final String outputYmlFile;
    private final String tableName;
    private final String separator;

    private final FileReader fileReader;
    private final YamlWriter yamlWriter;

    public DataMapper(String tableName, String inputCsvFile, String outputYmlFile, String separator) {
        this.inputCsvFile = inputCsvFile;
        this.tableName = tableName;
        this.outputYmlFile = outputYmlFile;
        this.separator = separator;

        fileReader = new FileReader();
        yamlWriter = new YamlWriter();
    }

    public void mapDataFromCsvToJson() {
        InputStream inputStream = fileReader.getFileFromResourceAsStream(inputCsvFile);
        List<String> lines = fileReader.readLines(inputStream);

        List<Map<String, String>> convertedLines = convertLinesToPairs(lines);
        yamlWriter.writeDataToYaml(outputYmlFile, tableName, convertedLines);
    }

    private List<Map<String, String>> convertLinesToPairs(List<String> lines) {
        List<Map<String, String>> convertedLines = new ArrayList<>();

        String[] columnNames = new String[0];

        boolean convertingHeaderRow = true;
        for (int lineIndex = 0; lineIndex < lines.size(); lineIndex++) {
            String line = lines.get(lineIndex);

            if (convertingHeaderRow) {
                columnNames = parseLine(line);
                convertingHeaderRow = false;
            } else {
                String[] values = parseLine(line);

                if (columnNames.length != values.length) {
                    throw new IllegalArgumentException(
                            String.format("Error while converting line %d. Actual number of values (%d) is different from expected number of columns (%d).  Please review your input file at this line.",
                                    lineIndex + 1,
                                    values.length,
                                    columnNames.length)
                    );
                }

                Map<String, String> rowData = new LinkedHashMap<>();

                for (int columnIndex = 0; columnIndex < columnNames.length; columnIndex++) {
                    rowData.put(columnNames[columnIndex], values[columnIndex]);
                }

                convertedLines.add(rowData);
            }
        }

        return convertedLines;
    }

    public String[] parseLine(String line) {
        if (StringUtils.isBlank(line)) {
            return new String[0];
        }

        return line.split(separator);
    }
}