# How to use

## 1. Source data
Export data from your database as CSV (including headers) and paste it into `src/main/resources/input.csv`.

## 2. Configuration
Open `Run.java` and provide configuration data:

   * Table name for which you're creating data.
   * Input file name (in case you renamed it from `input/input.csv`).
   * Output file name (by default, the table name will be used).
   * CSV separator

## 3. Outcome
Your data is available as a `src/main/resources/output/*.yml` file, ready to copy and paste to the dataset of your DBUnit / ... test resource.